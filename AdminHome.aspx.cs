﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminHome : System.Web.UI.Page
{
    public string tableType = "";

    protected void Page_Load(object sender, EventArgs e)
    {
      

        if(Session["usrID"] == null)
        {
            Response.Redirect("login.aspx");
        }
        if (!(bool)Session["isAdmin"])
        {
            Response.Write("<script>alert('שגיאה. אינך רשום כמנהל.')</script>");
            
        }
       
        
        tableType = Request.QueryString["type"];
        if(tableType == null)
        {
            string fullurl = "AdminHome.aspx?type=usr";
            foreach (string key in Request.QueryString)
                fullurl += "&" + key +"="+ Request.QueryString[key];          
            Response.Redirect(fullurl);
        }
        
    }
    public string GetOptionsArea(string sql, string selected)
    {
        string finalString = "";
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);


        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {
                string optGroup = "";

                while (oReader.Read())
                {
                    if (optGroup == "" || oReader.GetString(0) != optGroup)
                    {
                        optGroup = oReader.GetString(0);
                        finalString += "</optgroup><optgroup label='" + optGroup + "'>";
                    }

                    bool isSelected = false;
                    if (selected != null)
                    {

                        if (selected == oReader.GetInt32(1).ToString())
                        {
                            isSelected = true;
                        }

                    }
                    if (isSelected)
                    {
                        finalString += "<option value=" + oReader.GetInt32(1) + " selected>" + oReader.GetString(2) + "</option>";
                    }
                    else
                    {
                        finalString += "<option value=" + oReader.GetInt32(1);
                        finalString += ">" + oReader.GetString(2) + "</option>";

                    }


                }

                conn.Close();
            }

            com.Dispose();
            conn.Close();

        }
        return finalString;
    }

    public string showTable()
    {
        bool runQuery = true;
        string sql = "";
        string finalString = "";
        if (tableType == "usr")
        {
            sql = "SELECT id,fname,lname,email,pass,uname,tel FROM users";
            if (Request.QueryString["area"] != null)
                sql += " WHERE livein = " + Request.QueryString["area"];
            sql += " order by id";
            if (Request.QueryString["order"] == "1")
                sql += " DESC";
            finalString += " <table class='table table-bordered'><thead><tr> ";
            finalString += " <th scope='col'>#</th>";
            finalString += " <th scope='col'>שם פרטי</th>";
            finalString += " <th scope='col'>שם משפחה</th>";
            finalString += " <th scope='col'>אימייל</th>";
            finalString += " <th scope='col'>סיסמה</th>";

            finalString += " <th scope='col'>שם משתמש</th>";
            finalString += " <th scope='col'>פלאפון</th>";
            finalString += " <th scope='col'>&nbsp;&nbsp&nbsp;</th>";
            finalString += " <th scope='col'>&nbsp;&nbsp&nbsp;</th></tr></thead><tbody>";
        }
        else if (tableType == "favor")
        {
            sql = "SELECT favorId,price,subName,timeCreated,timeEvent,fname,lname,AreaName FROM QGetFavor order by favorId";
            finalString += " <table class='table table-bordered'><thead><tr> ";
            finalString += " <th scope='col'>#</th>";
            finalString += " <th scope='col'>מחיר</th>";
            finalString += " <th scope='col'>תת נושא</th>";
            finalString += " <th scope='col'>תאריך יצירה</th>";
            finalString += " <th scope='col'>תאריך</th>";

            finalString += " <th scope='col'>שם משתמש</th>";
            finalString += " <th scope='col'>מיקום</th>";

            finalString += " <th scope='col'>&nbsp;&nbsp&nbsp;</th>";
            finalString += " <th scope='col'>&nbsp;&nbsp&nbsp;</th></tr></thead><tbody>";
        }else
        {
            finalString += "<div class='alert alert-danger'>שגיאה! אנא נסו שוב מאוחר יותר.</div>";
            runQuery = false;
        }




        if (runQuery)
        {
            string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
            path += "db.accdb";
            //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
            string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
            OleDbConnection conn = new OleDbConnection(connString);


            conn.Open();
            OleDbCommand com = new OleDbCommand(sql, conn);
            OleDbDataReader data = com.ExecuteReader();
            bool found;
            found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
            data.Close();
            if (found)
            {
                //Found user matching
                using (OleDbDataReader oReader = com.ExecuteReader())
                {


                    while (oReader.Read())
                    {
                        /*

                       <tr>
                           <th scope="row">1</th>
                           <td>Mark</td>
                           <td>Otto</td>
                           <td>@mdo</td>
                       </tr>
                       <tr>
                           <th scope="row">2</th>
                           <td>Jacob</td>
                           <td>Thornton</td>
                           <td>@fat</td>
                       </tr>
                       <tr>
                           <th scope="row">3</th>
                           <td colspan="2">Larry the Bird</td>
                           <td>@twitter</td>
                       </tr>
                   </tbody>
               </table>
               */
                        if (tableType == "usr")
                        {
                            finalString += "<tr>";
                            finalString += "<th scope='row'>" + oReader.GetInt32(0) + "</th>";
                            finalString += "<td>" + oReader.GetString(1) + "</td>";
                            finalString += "<td>" + oReader.GetString(2) + "</td>";
                            finalString += "<td>" + oReader.GetString(3) + "</td>";
                            finalString += "<td>" + oReader.GetString(4) + "</td>";

                            finalString += "<td>" + oReader.GetString(5) + "</td>";
                            finalString += "<td>" + oReader.GetString(6) + "</td>";

                            finalString += "<td><a href='editUser.aspx?id=" + oReader.GetInt32(0) + "'>עריכה</a></td>";
                            finalString += "<td><a href='deleteUsr.aspx?id=" + oReader.GetInt32(0) + "'>מחיקה</a></td>";


                            finalString += "</tr>";
                        }
                        else if (tableType == "favor")
                        {
                            finalString += "<tr>";
                            finalString += "<th scope='row'>" + oReader.GetInt32(0) + "</th>";
                            finalString += "<td>" + oReader.GetInt32(1) + "</td>";
                            finalString += "<td>" + oReader.GetString(2) + "</td>";
                            finalString += "<td>" + oReader.GetDateTime(3).ToString("dd/MM/yy") + "</td>";
                            finalString += "<td>" + oReader.GetDateTime(4).ToString("dd/MM/yy") + "</td>";

                            finalString += "<td>" + oReader.GetString(5) + " " + oReader.GetString(6) + "</td>";
                            finalString += "<td>" + oReader.GetString(7) + "</td>";



                            finalString += "<td><a href='editFavor.aspx?id=" + oReader.GetInt32(0) + "'>עריכה</a></td>";
                            finalString += "<td><a href='deleteFavor.aspx?id=" + oReader.GetInt32(0) + "'>מחיקה</a></td>";


                            finalString += "</tr>";
                        }




                    }

                    conn.Close();
                }

                com.Dispose();
                conn.Close();

            }
        }
        finalString += "</tbody></table>";
        return finalString;

    }

    protected void dontShowModal_ServerChange(object sender, EventArgs e)
    {
        Session["IsAlreadyLoad"] = true;
        Response.Write("sdfsdf");
        Response.Redirect("Sdff");
    }
}