﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DeleteMsg : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["MID"] == null || !(Request.QueryString["MID"] is string))
        {
             Response.Write("<div class='alert alert-danger'>תקלה במחיקה של ההודעה... נסה שוב מאוחר יותר.</div>");
        }
        else
        {
            string sql = "SELECT buyerId FROM QGetMsgBuyer WHERE msgId = " + Request.QueryString["MID"];
            string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
            path += "db.accdb";
            //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
            string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
            OleDbConnection conn = new OleDbConnection(connString);
            conn.Open();
            OleDbCommand com = new OleDbCommand(sql, conn);
            OleDbDataReader data = com.ExecuteReader();
            bool found;
            found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
            data.Close();
            if (found)
            {
                //Found user matching
                using (OleDbDataReader oReader = com.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        if (oReader.GetInt32(0) == (int)Session["usrID"])
                        {
                            Dbb.DoQuery("db.accdb", "DELETE FROM usrMsg WHERE msgId = " + Request.QueryString["MID"]);
                            Response.Redirect("NewMsg.aspx");
                        }
                    }
                    conn.Close();
                }
                com.Dispose();
                conn.Close();
            }
            else
            {
                Response.Write("<div class='alert alert-danger'>תקלה במחיקה של ההודעה... נסה שוב מאוחר יותר.</div>");
            }
        }
    }
}
