﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class deleteFavor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((bool)Session["isAdmin"])
        {
            string sql = "DELETE FROM favorsUnlisted WHERE favorId =" + Request.QueryString["id"];
            Dbb.DoQuery("db.accdb", sql);
            Response.Redirect("AdminHome.aspx?type=favor");
        }
    }
}