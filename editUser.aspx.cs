﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class editUsr : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Request.Form["submit"] != null)
        {
            
           
            string updateUSRSQL = "UPDATE users SET ";
            foreach (string key in Request.Form)
            {

                if (key != "submit")
                {
                    if (Request.Form[1] != Request.Form[key])
                     updateUSRSQL += ",";
                    updateUSRSQL +=  key + " = '" + Request.Form[key] + "'";
                }
               
            }
            updateUSRSQL += " WHERE id = " + Request.QueryString["id"];
            Dbb.DoQuery("db.accdb", updateUSRSQL);
            Response.Redirect("AdminHome.aspx");
        }
    }
    public string showUsrFields()
    {
        string finalString = "";
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);
        string sql = "SELECT * FROM users WHERE id =" + Request.QueryString["id"];

        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {


                while (oReader.Read())
                {
                    for (int i = 1; i < 6; i++)
                    {

                        
                        string queryToAdd = "";
                        finalString += "<div class='form-group col-6'>";
                        string sql1 = "SELECT fieldTxt,queryName FROM fieldsText WHERE fieldId = " + i;
                        OleDbCommand com1 = new OleDbCommand(sql1, conn);
                        using (OleDbDataReader oReaded1 = com1.ExecuteReader())
                        {
                            while (oReaded1.Read())
                            {
                                finalString += "<label>" + oReaded1.GetString(0) + "</label>";
                                if (oReaded1.GetString(1) != "0")
                                    queryToAdd = oReaded1.GetString(1);


                                switch (oReader.GetFieldType(i).ToString())
                                {
                                    case "System.String":
                                        finalString += "<input type='text' name='" + oReaded1.GetString(1) +"' value='" + oReader.GetValue(i) + "' class='form-control'/></div>";
                                        break;
                                    case "System.Int32":
                                        finalString += "<input type='text' name='" + oReaded1.GetString(1) + "' value='" + oReader.GetValue(i) + "' class='form-control' /></div>";
                                        break;
                                    case "System.Boolean":
                                        if (oReader.GetBoolean(i))
                                            finalString += "<select  class='form-control' name='" + oReaded1.GetString(1) + "' value='" + oReader.GetValue(i) + "'><option value='True' selected>כן</option><option value='False'>לא</option></select></div>";
                                        else
                                            finalString += "<select  class='form-control' name='" + oReaded1.GetString(1) + "' value='" + oReader.GetValue(i) + "'><option value='True'>כן</option><option value='False' selected>לא</option></select></div>";

                                        break;


                                    case "System.DateTime":
                                        finalString += "<input type='text' class='form-control selectDate' name='" + i + "' value='" + oReader.GetValue(i) + "'/></div>";
                                        break;
                                }
                            }
                        }

                    }
                }
                conn.Close();
            }

            com.Dispose();
            conn.Close();
        }

        return finalString;

    }
}