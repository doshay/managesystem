﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.master" AutoEventWireup="true" CodeFile="AdminHome.aspx.cs" Inherits="AdminHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%=Application["UsersCounter"] %>
      
    <div class="modal fade" id="onStart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ברוך הבא מנהל !</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    המשתמש שלך רשום במערכת כמנהל, מהמסך הזה תוכל לנהל את המשתמשים ואת המידע שלהם.
                </div>
                <div class="modal-footer">
                                <button type="submit" name="submit" class="btn btn-secondary btn-block" data-dismiss="modal">הבנתי</button>
 
                </div>
            </div>
        </div>
    </div>
    
    <%if (Session["IsAlreadyLoad"] == null)
    {
            Session["IsAlreadyLoad"] = true;
        Response.Write("<script>$(document).ready(function () {$('#onStart').modal()})</script>");
    } %>

    <div class="row">
        <div class="col-2 offset-05 mt-3 rounded" style="background-color: white; min-height: 40rem;">
            <div class="container pt-4" style="color:black;">
              <form method="get" autocomplete="off">

                        
                        <div class="form-group">
                            <label for="expertLevel">אזור מגורים</label>
                            <select name="area" id="area" class="form-control">
                               <%=GetOptionsArea("SELECT * FROM QGetAreas order by districtID", null) %>
                            </select>
                        </div>
                        <hr class="my-4" style="border-top: 1px solid black">                        
                        <div class="form-group">
                            <label for="orderSelect">מיין לפי - </label>
                            <select class="form-control" id="orderSelect" name="order">
                                <option value="0">תאריך הצטרפות(ותיק למעלה)</option>
                                <option value="1">תאריך הצטרפות(חדש למעלה)</option>                            
                            </select>
                        </div>
                        <button type="submit"   id="submit" class="btn btn-warning btn-block mt-4">סנן</button>
                        <a role="button" href="AdminHome.aspx?type=usr"  class="btn btn-secondary btn-block mt-4">איפוס</a>

                    </form>
            </div>
        </div>
        <div class="col-8">
            <div class="container-fluid rounded mt-3 p-2" style="background-color: white; min-height: 40rem;">
                <div class="container">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link <%if (Request.QueryString["type"] == "usr") Response.Write("active"); %>" href="AdminHome.aspx?type=usr">עריכת משתמשים</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <%if (Request.QueryString["type"] == "favor") Response.Write("active"); %>" href="AdminHome.aspx?type=favor">ניהול הזמנות</a>
                        </li>

                    </ul>
                </div>
                <div class="container-fluid pt-1">
                    <%if ((bool)Session["isAdmin"]) Response.Write(showTable());%>
                </div>

            </div>
        </div>

    </div>
</asp:Content>

