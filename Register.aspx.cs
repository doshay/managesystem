﻿using Nexmo.Api;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

public partial class Register : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        
        AlertMsg.Visible = false; //חלון שגיאות נסגר כשהאתר נטען 

        /*
        if (Request.Form["submit"] != null) // כאשר נשלח הטופס
        {
            bool trySubmit = true;
            string email = Request.Form["email"];
            string pass = Request.Form["pass"];
            string fname = Request.Form["fname"];
            string lname = Request.Form["lname"];
            string uname = Request.Form["uname"];
            string friend = Request.Form["friendCode"];
            string address = Request.Form["address"];
            string phone = Request.Form["phone"];


            if (Dbb.IsExist("db.accdb", "SELECT * FROM users WHERE uname = '" + uname + "'")) // אם השם משתמש קיים במערכת הצגת שגיאה
            {
                AlertMsg.Visible = true;
                AlertMsg.InnerHtml += "<div class='alert alert-warning'  role='alert' id='unameAlert'>השם משתמש כבר קיים במערכת נסה שם אחר.</div>";
                trySubmit = false;
            }
            if (Dbb.IsExist("db.accdb", "SELECT * FROM users WHERE tel = '" + phone + "'")) //אם מספר הפלאפון קיים במערכת שגיאה
            {
                AlertMsg.Visible = true;
                AlertMsg.InnerHtml += "<div class='alert alert-warning' role='alert' id='unameAlert'>הפלאפון כבר קיים במערכת נסה מספר אחר.</div>";
                trySubmit = false;
            }
            if (Dbb.IsExist("db.accdb", "SELECT * FROM users WHERE email = '" + email + "'")) // אם האימייל קיים במערכת שגיאה
            {
                AlertMsg.Visible = true;

                AlertMsg.InnerHtml += "<div class='alert alert-warning' role='alert' id='emailAlert'>האימייל כבר קיים במערכת נסה כתובת אחרת.</div>";
                trySubmit = false;

            }
            if (trySubmit) // אם לא הוצגה שגיאה השרת מנסה להכניס את המשתמש למסד הנתונים
            {
                string SQL = "INSERT INTO users(fname,lname,email,uname,pass,tel,points,dateCreated,liveIn) VALUES('";
                SQL += fname + "','";
                SQL += lname + "','";
                SQL += email + "','";
                SQL += uname + "','";
                SQL += pass + "','";

                SQL += phone + "',";
                SQL += 30 + ",";
                SQL += DateTime.Now.ToString("d/M/yyyy") + ",";
                SQL += address + ")";

                Dbb.DoQuery("db.accdb", SQL);
            
                SuccessMsg.Visible = true;
                SuccessMsg.InnerHtml += "<div class='alert alert-success' role='alert' id='emailAlert'>המשתמש נוצר בהצלחה</div>";
                AlertMsg.Visible = false;
            }
           
        }
         */
    }





    public string GetOptionsArea(string sql, string selected)
    {
        string finalString = "";
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);


        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {
                string optGroup = "";
                while (oReader.Read())
                {
                    if (optGroup == "" || oReader.GetString(0) != optGroup)
                    {
                        optGroup = oReader.GetString(0);
                        finalString += "</optgroup><optgroup label='" + optGroup + "'>";
                    }
                    bool isSelected = false;
                    if (selected != null)
                    {

                        if (selected == oReader.GetInt32(1).ToString())
                        {
                            isSelected = true;
                        }
                    }
                    if (isSelected)
                    {
                        finalString += "<option value=" + oReader.GetInt32(1) + " selected>" + oReader.GetString(2) + "</option>";
                    }
                    else
                    {
                        finalString += "<option value=" + oReader.GetInt32(1);
                        finalString += ">" + oReader.GetString(2) + "</option>";

                    }


                }

                conn.Close();
            }

            com.Dispose();
            conn.Close();

        }
        return finalString;
    }

}