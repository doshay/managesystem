﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <title>LLogin</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/signin.css" rel="stylesheet" />
    <link href="css/Main.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet" />

    <script>
        function showAlert(alert) {
            document.getElementById("AlertMsgClient").style.display = 'block';
            document.getElementById("AlertMsgClient").innerHTML = alert;
        }
        function CheckPassword(pass) {

            var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
            if (!pass.match(passw)) {
                showAlert("הסיסמה לא מכילה את התווים הנדרשים.");
                return false;
            }
        }
       
        function validateForm() {

            if (CheckPassword(document.getElementById("pass").value) == false) {
                return false;
            }
           
        }

    </script>
</head>
<body class="text-center">
    <div class="container">
        <h1 class="display-1 font-weight-bold text-center pb-4">ברוכים הבאים למערכת הניסיונית של "עשו טובה"</h1>
    </div>
    <form class="form-signin" method="post"  onsubmit="return validateForm()" autocomplete="off">
        <h1 class="h3 mb-3 font-weight-normal text-white">אנא הכנס שם משתמש וסיסמה</h1>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="text" id="email" name="email" class="form-control" placeholder="אימייל או שם משתמש"/>
        <label for="inputPassword" class="sr-only">Password</label>
       <input type="password" class="form-control" name="pass" id="pass" placeholder="השתמשו בסיסמה חזקה!" />


        <button name="submit" class="btn btn-lg btn-primary btn-block" type="submit">כניסה</button>
        <a role="button" class="btn btn-lg btn-warning btn-block" href="register.aspx">עוד לא נרשמת למערכת?</a>
         <div class="container-fluid mt-2 p-2 alert alert-danger" style="display:none;"  id="AlertMsgClient">
            
        </div>
          <div class="container-fluid mt-2 p-2 alert alert-danger" style="display:none;" runat="server" id="AlertMsgServer">
            
        </div>
    </form>
     
    <div class="fixed-bottom pb-4">
        <p class="text-muted">Shay Livni &#174;</p>

    </div>

</body>

</html>
