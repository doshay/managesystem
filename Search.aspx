﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Search1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <h1 class="display-1 font-weight-bold text-center pb-4" style="font-size: 70px; color: white;">חפש מישהו שיעזור לך!</h1>
        <form method="get" class="form-row align-items-center" autocomplete="off">
            <div class="col-3">
                <select class="form-control" id="subjectField" name="subjectField" onchange="subPop()">
                    <option class="font-weight-bold" value="null">תחום כללי </option>
                    <% Response.Write(GetOptions("SELECT * FROM subjects order by subjectId", Request.QueryString["subjectField"])); %>
                </select>
                <script>
                    function subPop() {
                        var MainSubject = document.getElementById('subjectField').value;
                        document.getElementById("valOfSubject").value = MainSubject;
                        var SideServerBtn__SUBSubject = document.getElementById("SideServerBtn__SUBSubject");
                        sub.click();
                    }
                </script>
            </div>
            <div class="col-3">
                <button style="display: none;" id="SideServerBtn__SUBSubject" runat="server" onserverclick="SideFunctionLoadSub">SERVERLAYOUT__</button>
                <input type="hidden" id="valOfSubject" value="0" />
                <select class="form-control" id="subField" name="subField">
                    <option class="font-weight-bold" value="null">תת תחום</option>
                    <%
                        if (Request.QueryString["subjectField"] != null)
                        {
                            Response.Write(SERVERSIDE_SubjectVal);
                        }
                        else
                        {
                            Response.Write("<option value='00'>אנא בחר תחום כללי</option>");
                        }
                    %>
                </select>
            </div>
            <div class="col-2 pt-3" dir="rtl">
                <select class="form-control" id="cityField" name="cityField">
                    <option class="font-weight-bold" value="null">אזור בארץ</option>
                    <%
                       
                        Response.Write(GetOptionsArea("SELECT * FROM QGetAreas order by districtID",Request.QueryString["cityField"])); %>
                </select>
                <p>
                </p>
            </div>
            
                <div class="col-2">
                    <input class="form-control selectDate" value="<%=CheckDate("FromDateField") %>"  type="text" id="FromDateField" name="FromDateField" size="30" placeholder="מתאריך" />
                    <input class="form-control selectDate mt-2" type="text"  value="<%=CheckDate("ToDateField") %>" id="ToDateField" name="ToDateField" size="30" placeholder="עד תאריך" />

                   
                </div>
               
          

            <div class="col-2">
                <button type="submit" id="sub" name="submit" class=" btn btn-warning btn-block">חפש טובה !</button>
            </div>

        </form>
    </div>
    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-2 offset-05">
                <div class="container-fluid rounded p-4 shadow-sm " style="background-color: #553d67; height: 30rem;">

                    <form method="get" autocomplete="off">

                        <%
                            string full = "";
                            foreach (string x in Request.QueryString)
                            {
                                full += "<input type='hidden' name='" + x + "' value = '" + Request.QueryString[x] + "'/>";
                            }
                            Response.Write(full);

                        %>
                        <div class="form-group">
                            <label for="expertLevel">ניסיון</label>
                            <select id="expertLevel" class="form-control">
                                <option value="null">אל תמיין</option>

                                <%Response.Write(GetOptions("SELECT [level], expName FROM expLevel order by [level]", "")); %>
                            </select>
                        </div>
                        <hr class="my-5" style="border-top: 1px solid #ffffff" />
                        <label>עלות נקודות</label>
                        <div class="form-row">

                            <div class="form-group col-6">
                                <input type="text" name="fromPoints" id="fromPoints" class="form-control" placeholder="מ-" />
                            </div>
                            <div class="form-group col-6">
                                <input type="text" name="toPoints" id="toPoints" class="form-control" placeholder="עד-" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="orderSelect">מיין לפי - </label>
                            <select class="form-control" id="orderSelect" name="orderSelect">
                                <option value="null">אל תמיין</option>
                                <%Response.Write(GetOptions("SELECT orderId,orderName FROM orderQ", "")); %>
                            </select>

                        </div>

                        <button type="submit" name="submit" id="submit" class="btn btn-warning btn-block mt-4"
                            <%if (!queryFilled)
                            {
                                Response.Write("disabled");
                            }
                           %>>
                            סנן</button>
                    </form>

                </div>
            </div>
            <div class="col-8">
                <div class="container-fluid rounded" style="background-color:#99738e; height:300%">
                    <div class="row">
                        
                        <%
                            bool subject = false;
                            bool sub = false;
                            bool city = false;
                            bool date = false;
                            string sql = "SELECT * FROM QGetFavor WHERE deleted = FALSE AND id <> "+Session["usrID"];

                            if (!(Request.QueryString["subjectField"] == null || Request.QueryString["subjectField"] == "null"))
                            {
                                sql += " AND SubjectId = " + Request.QueryString["subjectField"] ;
                                subject = true;
                            }

                            //check if query exist

                            if (!(Request.QueryString["subField"] == null || Request.QueryString["subField"] == "null"))
                            {
                                if (!subject)
                                {
                                    sql += " AND ";
                                }else
                                {
                                    sql += " AND ";
                                }
                                sql += "subId = " + Request.QueryString["subField"];
                                sub = true;
                            }
                            if (!(Request.QueryString["cityField"] == null || Request.QueryString["cityField"] == "null"))
                            {
                                if (!subject && !sub)
                                    sql += " AND ";
                                else
                                    sql += " AND ";
                                sql += " AreaId = " + Request.QueryString["cityField"];
                                city = true;
                            }
                            DateTime today = DateTime.Today;
                            if(DateTime.TryParse(Request.QueryString["ToDateField"],out today) && DateTime.TryParse(Request.QueryString["FromDateField"],out today) )
                            {
                                if (!sub && !subject && !city)
                                {
                                    sql += " AND ";

                                }
                                else
                                    sql += " AND ";
                                sql += " timeEvent BETWEEN #" + Request.QueryString["FromDateField"] + "# AND #" + Request.QueryString["ToDateField"] +"#";
                            }





                            
                             Response.Write(getFavorsList(sql)); %>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

