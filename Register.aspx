﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>הרשמה למערכת</title>

    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/Main.css" rel="stylesheet" />

   <script>
       function showAlert(alert) {
           document.getElementById("AlertMsgClient").style.display = 'block';
           document.getElementById("AlertMsgClient").innerHTML = alert;
       }
       function checkTel(tel) {
           var phoneRegax = /^\d{10}$/;
           if (!tel.match(phoneRegax)) {
               showAlert("מספר הפלאפון שהוזן לא תקין");
               return false;
           }
       }
       function checkName(name) {
           if (name == "") {
               showAlert("שדה השם הוא שדה חובה");
               return false;
           }
           var regex = /\d/g;
           if (name.match(regex)) {
               showAlert("אנא תקן/י את השם (שם לא יכול להכיל מספרים)");
               return false;
           }
           for (i = 0; i < name.length; i++) {
               if (!(name[i] <= 'ת' && name[i] >= 'א')) {
                   showAlert("אנא תקן/י את השם (שם יכול להכיל רק אותיות בעברית)");

                   return false;
               }
           }
          
       }
       function CheckPassword(pass) {
          
               var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
               if (!pass.match(passw)) {
                   showAlert("הזן סיסמה באנגלית בעלת 6-20 תווים שמכילה לפחות מספר ואות גדולה אחת.");
                   return false;
               }
       }
       function checkEmail(email) {
           if (email.includes('@') == false) {
               showAlert("כתובת אימייל לא חוקית.");
               return false;
           }
           if (email.includes('.') == false) {
               showAlert("כתובת אימייל לא חוקית.");
               return false;
           }
           if ((name[i] <= 'ת' && name[i] >= 'א')) {
               showAlert("כתובת אימייל לא חוקית.");
               return false;
           }
       }
       function validateForm() {

           if (checkName(document.getElementById("fname").value) == false) {
               return false;
           }
           if (checkEmail(document.getElementById("email").value) == false) {
               return false;
           }
           if (CheckPassword(document.getElementById("pass").value) == false) {
               return false;
           }
           if (checkTel(document.getElementById("phone").value) == false) {
               return false;
           }
       }
   </script>
</head>
<body>
    <div class="container-fluid pt-4">
        <h1 class="display-1 font-weight-bold text-center pb-4">הרשמה למערכת</h1>
    </div>
    <div class="container">
        <form class="form-signin"  onsubmit="return validateForm()" method="post" autocomplete="off" >

            <div class="card" style="width: auto;">
                <h5 class="card-header text-white " style="background-color: #99738e;">פרטי המשתמש
                   
                    <img src="img/icons/doc.png" class="icon img-fluid" style="height: 1.9rem; width: 1.9rem;" /></h5>
                <div class="card-body">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">כתובת אימייל</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="abcde@abc.com" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">מס' פלאפון</label>
                            <div class="input-group-prepend">
                                <input type="tel" class="form-control" id="phone" name="phone" placeholder="0501234567" />
                            </div>
                        </div>
                        <p class="text-muted">*פרטים אלו יאומתו ורק לאחר אימות תוכל להשתמש במערכת.</p>
                    </div>
                    <hr class="m-4" />
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputAddress2">שם משתמש</label>
                            <input type="text" class="form-control" name="uname" id="uname" placeholder="Israel123" />
                        </div>
                         <div class="form-group col-md-6">
                            <label for="inputAddress2">סיסמה</label>
                            <input type="password" class="form-control" name="pass" id="pass" placeholder="השתמשו בסיסמה חזקה!" />
                        </div>
                    </div>
                    
                     <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputAddress2">שם פרטי</label>
                            <input type="text" class="form-control" name="fname" id="fname" placeholder="ישראל" />
                        </div>
                           <div class="form-group col-md-6">
                            <label for="inputAddress2">שם משפחה</label>
                            <input type="text" class="form-control" name="lname" id="lname" placeholder="ישראל" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-7">
                            <label for="inputAddress2">אזור מגורים</label>
                            <select  class="form-control" name="address" id="address"  >
                                <%Response.Write(GetOptionsArea("SELECT * FROM QGetAreas order by districtID",Request.QueryString["cityField"])); %>
                            </select>
                        </div>
                        <div class="form-group col-md-5">
                            <label for="inputAddress2">קוד חבר</label>
                            <input value="<%if (Request.QueryString["href"] != null) Response.Write(Request.QueryString["href"]); %>" class="form-control" name="friendCode" id="friendCode"  />

                            
                        </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-block btn-warning">הרשמה</button>
               </div>

                
                <div class="container-fluid p-2" runat="server" id="AlertMsg">
            
        </div>
                 <div class="container-fluid p-2 alert alert-danger" style="display:none;"  id="AlertMsgClient">
            
        </div>
                <div class="container-fluid p-2" runat="server" id="SuccessMsg">
            
        </div>
            </div>
        </form>
        
    </div>


    <div class="fixed-bottom pb-4">
        <p class="text-muted">Shay Livni &#174;</p>

    </div>

</body>
</html>
