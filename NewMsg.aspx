﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.master" AutoEventWireup="true" CodeFile="NewMsg.aspx.cs" Inherits="NewMsg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container-fluid">
        <div class="row">
            <div class="col-4">
                        <h1 class="display-1 font-weight-bold text-center pb-4" style="font-size: 70px; color: white;">הודעות</h1>

            </div>
            <div class="col-8">
                                        <h1 class="display-1 font-weight-bold text-center pb-4" style="font-size: 70px; color: white;">פניות</h1>

            </div>
        </div>

        <div class="row">
            <div class="col-4 offset-01">
                
                    <%=getNewMsg() %>
           
            </div>    
            <div class="col-8">
           

                <%=getNewFavors() %>
                <div>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">אישור העברה</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form method="post">
                                        <div class="form-group">

                                            <p>ברגע שתבצע אישור הזמנה יעברו <span class="font-weight-bold" id="points"></span>נקודות מהחשבון שלך אל <span id="usrSeller"></span></p>

                                        </div>

                                        <div class="form-group">
                                            <label for="message-text" class="col-form-label">הודעה למוכר(אופציונלי)</label>
                                            <textarea class="form-control" id="message-text" name="msg-text"></textarea>
                                            <input type="hidden" name="favorId" id="IDFavor__" />
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">ביטול</button>
                                            <button type="submit" name="submit" class="btn btn-primary">שליחה</button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                
            </div>
            </div>
    </div>
    <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var points = button.data('points')
            var seller = button.data('seller')
            var id = button.data('id')

            var modal = $(this)
            modal.find('.modal-title').text('שליחת אישור הזמנה ל - ')
            modal.find('#points').text(points)
            modal.find('#usrSeller').text(seller)
            modal.find('#IDFavor__').val(id)

        })
        </script>
    </div>
</asp:Content>

