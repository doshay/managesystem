﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FavorListed : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string action;
        if (Request.QueryString["action"] ==  null)
            action = "none";
        else
            action = "add";
        SendToSeller();

    }
    public string get()
    {

        String finalString = "";
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);

        string sql = "SELECT price,subName,AreaName,fname,lname,timeEvent,[level],timesRated,tel,email,usrAreaName,expName,dateCreated,describeText FROM QGetFavor WHERE favorId = "+Request.QueryString["FID"]+"";
        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {


            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {
                while (oReader.Read())
                {

                    /* 
                <div class="container p-3 mb-2 border">
                    <h4 class="font-weight-bold pb-2" style="font-size: 1.7rem;">פרטי ההזמנה </h4>
                    <div class="container" style="font-size: 110%;">
                     * <div class="row">
                            <div class="col-4">
                                <p><span class="font-weight-bold">מחיר אחרון -</span>&nbsp;400 </p>
                                <p><span class="font-weight-bold">תת-תחום -</span>&nbsp;400 </p>
                                <p><span class="font-weight-bold">אזור בארץ -</span>&nbsp;400 </p>


                            </div>
                            <div class="col-6">
                                <p><span class="font-weight-bold">שם מלא  -</span>&nbsp;400 </p>

                                <p><span class="font-weight-bold">סוג הזמנה -</span>&nbsp;400 </p>
                                <p><span class="font-weight-bold">תאריך ביצוע -</span>&nbsp;400 </p>
                            </div>
                        </div>
                        */
                    finalString += " <div class='container p-3 mb-2 border'>";
                    finalString += "<h4 class='font-weight-bold pb-2' style='font-size:1.7rem;'>פרטי ההזמנה </h4>";
                    finalString += " <div class='container' style='font-size:110%;'>";
                    finalString += "<div class='row'>";
                    finalString += "<div class='col-4'>";
                    finalString += "<p><span class='font-weight-bold'>מחיר אחרון -</span><img src='img/icons/coin-stack.png' class='img-fluid m-1' style='max-width:5%;height:auto'/>" + oReader.GetInt32(0) + "</p>";
                    finalString += "<p><span class='font-weight-bold'>תת-תחום -</span>&nbsp;" + oReader.GetString(1) + "</p>";
                    finalString += "<p><span class='font-weight-bold'>אזור בארץ -</span>&nbsp;" + oReader.GetString(2) + "</p>";
                    finalString += "</div> <div class='col-6'>";
                    finalString += "<p><span class='font-weight-bold'>שם מלא-</span>&nbsp;" + oReader.GetString(3) + " " + oReader.GetString(4) + "</p>";
                    finalString += "<p><span class='font-weight-bold'>סוג הזמנה -</span>&nbsp;" + "כדגכ" + "</p>";
                    finalString += "<p><span class='font-weight-bold'>תאריך ביצוע -</span>&nbsp;" + oReader.GetDateTime(5).ToString("d/M/yyyy") + "</p>";
                    finalString += "<p><span class='font-weight-bold'>טקסט חופשי -</span>&nbsp;" + oReader.GetString(13) + "</p>";

                    finalString += "</div></div></div></div>";

                    /*<div class="row">
                        <div class="col-3">
                            <h4 class="font-weight-bold pb-2" style="font-size: 1.7rem;">פרטי המוכר</h4>

                        </div>
                        <div class="col-5">
                            
                            <span class="
                            
                    fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            
                            <p class="text-white">42 דירוגים</p>
                        </div>
                    </div>
                    <div class="container" style="font-size: 110%;">

                        <div class="row">
                            <div class="col-4">
                                <p><span class="font-weight-bold">מספר פלאפון -</span>&nbsp;400 </p>
                                <p><span class="font-weight-bold">אימייל -</span>&nbsp;400 </p>
                                <p><span class="font-weight-bold">אזור מגורים -</span>&nbsp;400 </p>


                            </div>
                            <div class="col-6">
                                <p><span class="font-weight-bold">דרגה -</span>&nbsp;400 </p>
                                <p><span class="font-weight-bold">תאריך הרשמה -</span>&nbsp;400 </p>
                            </div>
                        </div>

                    </div>
                    */
                    finalString += "<div class='container p-3 border'>";
                    finalString += "<div class='row'>";
                    finalString += "<div class='col-3'>";                    
                    finalString += "<h4 class='font-weight-bold pb-2' style='font-size: 1.7rem;'>פרטי המוכר</h4> </div>";
                    finalString += "<div class='col-5'>";

                    int count = 0;
                    for(int i = 0; i < oReader.GetInt32(6); i++)
                    {
                        finalString += "<span class='fa fa-star checked'></span>";
                        count++;
                    }

                    for(int i = count; i < 5; i++)
                    {
                        finalString += "<span class='fa fa-star'></span>";

                    }
                    finalString += "<p class='text-white'> "+oReader.GetInt32(7)+"    דירוגים </p> </div></div>";
                    finalString += "<div class='container' style='font-size:110%;'>";
                    finalString += "<div class='row'>";
                    finalString += "<div class='col-4'>";
                    finalString += "<p><span class='font-weight-bold'>מספר פלאפון </span>&nbsp;" + oReader.GetString(8) + "</p>";
                    finalString += "<p><span class='font-weight-bold'>אימייל </span>&nbsp;" + oReader.GetString(9) + "</p>";
                    finalString += "<p><span class='font-weight-bold'>אזור בארץ -</span>&nbsp;" + oReader.GetString(10) + "</p>";
                    finalString += "</div> <div class='col-6'>";
                    finalString += "<p><span class='font-weight-bold'>דרגה-</span>&nbsp;" +oReader.GetString(11) + "</p>";
                    finalString += "<p><span class='font-weight-bold'>תאריך הרשמה למערכת -</span>&nbsp;" + oReader.GetDateTime(12).ToString("d/M/yyyy") + "</p>";
                    finalString += "</div></div>";
                }

                conn.Close();
            }

            com.Dispose();
            conn.Close();

        }
        return finalString;
    }

    public void SendToSeller()
    {
        int id = int.Parse(Request.QueryString["FID"]);
        string sql = "INSERT INTO FavorListed(unlistedId,buyerId,dateListed) VALUES(" + id;
        sql += ", " +int.Parse(Session["usrID"].ToString());
        sql += ", " + DateTime.Today.ToString("dd/MM/yy") +")";
        if (!Dbb.IsExist("db.accdb", "SELECT * FROM FavorListed WHERE unlistedId = " + id + " AND buyerId =" + int.Parse(Session["usrID"].ToString())))
        {
            Dbb.DoQuery("db.accdb", sql);
            
        }
        else
        {
            msgAfterSend.InnerHtml = "אופס... יש בעיה, נראה שכבר שלחת הודעה למוכר לגבי אותה הטובה.";
        }
       
    }
}
 
 