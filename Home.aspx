﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="p-4">



        <div style="color: white;" class="container p-4 p-md-5 font-weight-bold">
            <div class="col-md-6 px-0">
                <h1 style="font-family: Santica; font-size: 100px;" class="display-5 font-weight-bold">כל אחד צריך טובה'לה.</h1>
                <p class="lead my-3">עושים טובה, מקבלים נקודות ומרוויחים.</p>
                <div class="row pt-4">
                    <div class="col-3">
                        <a href="Search.aspx">
                            <img src="img/icons/search01.png" class="icon" /></a>
                    </div>
                    <div class="col-3">
                        <a href="NewFavor.aspx">
                            <img src="img/icons/add01.png" class="icon" /></a>
                    </div>

                </div>

            </div>
        </div>
       </div>

    <!--חלון קופץ להצגת הודעות חדשות -->
     <div  class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div  class=" modal-content ">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">הודעות חדשות <span class="badge badge-light"><%=getMsgCount() %></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                                     יש לך מספר הודעות חדשות תוכל/י להיכנס אליהן <a href="NewMsg.aspx">כאן</a>                          
                          
                     
                    </div>
                      <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">הבנתי</button>
                               
                            </div>
                </div>
            </div>
            
        </div>
    <script>

        //אם למשתמש יש הודעות חדשות (ע"פ הפעולה בצד שרת) השרת יוסיף קוד גאוה סקריפט כדי שיקפוץ חלון מודאל
        <% if ((bool)Session["HaveNewMsg"] && !(bool) Session["ReadNewMsg"]) { Response.Write("  $(document).ready(function () { $('#exampleModal').modal() })"); Session["ReadNewMsg"] = true; }%>
      
        </script>
</asp:Content>

