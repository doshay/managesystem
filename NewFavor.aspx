﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.master" AutoEventWireup="true" CodeFile="NewFavor.aspx.cs" Inherits="NewFavor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container" id="ASubjectPanel" runat="server">
        <div class="container">
            <h1 dir="rtl" style="font-size: 2.4rem;">
                <img src="img/icons/subjects/<%=imgH %>.png" class="icon m-3 p-2 img-fluid" /><%=subName %> - פרסום בקשה </h1>
            <hr class="my-4" style="border-top: 2.4px solid white" />

        </div>

        <div class="container">
            <div class="card" style="width: auto;">
                <h5 class="card-header text-white " style="background-color: #99738e;">פרטים כללים 
                    <img src="img/icons/doc.png" class="icon img-fluid" style="height: 1.9rem; width: 1.9rem;" /></h5>
                <div class="card-body">
                    <h5 class="card-title">פרטי המבקש</h5>
                    <p class="text-muted">הפרטים שלך יישארו במערכת ולא יועברו לאף אחד . </p>
                    <form name="form" method="post" id="usrTxt">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">כתובת אימייל</label>
                                <input value="<%=Session["usrEmail"] %>" type="email" class="form-control" name="email" id="email" disabled />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4">מס' פלאפון</label>
                                <div class="input-group-prepend">
                                    <input value="0<%=Session["usrTel"] %>" type="tel" class="form-control" id="phone" name="phone" aria-describedby="basic-addon1" disabled />


                                </div>
                            </div>
                        </div>

                        <hr class="m-4" />


                        <div class="form-row">
                            <div class="form-group col-md-7">
                                <label for="inputAddress2">כתובת מלאה(אופציונלי)</label>
                                <input type="text" class="form-control" name="address" id="address" placeholder="רחוב ישראל קומה 6 דירה 14" />
                            </div>
                            <div class="form-group col-md-4">
                                <label for="inputCity">עיר (אופציונלי)</label>
                                <input type="text" class="form-control" name="inputCity" id="inputCity" />

                                <p>Selected: <strong id="address-value">none</strong></p>
                                <script>
                                    (function () {
                                        var placesAutocomplete = places({
                                            appId: '86OLV0AN1V',
                                            apiKey: 'b672186073e69534d99b2d9b4dd23368',
                                            container: document.querySelector('#address')
                                        });

                                        var $address = document.querySelector('#address-value')
                                        placesAutocomplete.on('change', function (e) {
                                            $address.textContent = e.suggestion.value
                                        });

                                        placesAutocomplete.on('clear', function () {
                                            $address.textContent = 'none';
                                        });

                                    })();
                                </script>
                            </div>
                        </div>
                    </form>
                </div>


            </div>
            <div class="card" style="width: auto;">
                <h5 class="card-header text-white " style="background-color: #99738e;">פרטים על המודעה
                </h5>
                <div class="card-body">
                    <form method="post" id="adTxt">
                        <p class="text-muted">*יותר פרטים = יותר חשיפה! </p>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputAddress">אזור </label>
                                <select class="form-control" id="cityField" name="cityField">
                                    <option class="font-weight-bold" value="null">אזור בארץ</option>
                                    <%
                                        Response.Write(GetOptionsArea("SELECT * FROM QGetAreas order by districtID", null)); %>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputAddress">תאריך</label>
                                <input class="form-control selectDate" type="text" id="DateField" name="DateField" size="30" />

                            </div>
                            <script>
                                flatpickr(".selectDate", {
                                    locale: "he",
                                    dateFormat: "d/m/Y",
                                    allowInput: true
                                });
                            </script>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">שם קצר למודעה</label>
                                <input type="text" class="form-control" name="shortName" id="shortName" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">תת-תחום</label>
                                <select class="form-control" id="subField" name="subField">

                                    <%
                                        Response.Write(Request.QueryString["SID"]);
                                        Response.Write(GetOptions("SELECT * FROM subSubjects WHERE SubjectId = " + Request.QueryString["SID"], "1"));

                                    %>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="inputPassword4">תיאור המודעה (עד 60 מילים)</label>
                                <textarea class="form-control" id="freeTxt" name="freeTxt" aria-describedby="basic-addon1" rows="4"></textarea>
                            </div>

                        </div>

                        <hr class="m-4" />


                        <button type="submit" name="submit" class="btn btn-outline-warning btn-block" onclick="submitForms()">הוסף מודעה</button>
                    </form>
                </div>
            </div>

        </div>
        <script>
            submitForms = function () {
                document.getElementById("adTxt").submit();
                document.getElementById("usrTxt").submit();
            }
        </script>
    </div>
    <div class="container" id="SubjectPanel" runat="server">
        <div class="container-fluid">
            <h1 dir="rtl">
                <img src="img/icons/doc.png" class="icon m-3 p-2 img-fluid" />פרסום בקשה חדשה</h1>
            <p style="font-size: 1.3rem;" class="pl-5 ml-5 text-white">בחר מהתחומים הבאים את התחום המתקשר לטובה שלך.</p>
            <hr class="my-4" style="border-top: 2px solid white" />
        </div>

        <div class="container-fluid">
            <div class="row">

                <%
                    Response.Write(getSubjects("SELECT * FROM subjects")); %>
            </div>
        </div>

    </div>

</asp:Content>

