﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Home1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["usrID"] == null)
        {
            Response.Redirect("login.aspx");
        }
        checkNew();
    }

    //פעולה לבדיקה האם למשתמש יש הודעות חדשות, אם כן המשתנה בסשן ישתנה בהתאם
    public void checkNew()
    {
        string sql = "SELECT * FROM QGetMsgBuyer WHERE buyerId = " + Session["usrID"];

        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);
        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {
                while (oReader.Read())
                {
                    Session["HaveNewMsg"] = true;
                }
                conn.Close();
            }
            com.Dispose();
            conn.Close();
        }
    }

    // פעולה המחזירה את מספר ההודעות החדשות שיש למשתמש
    public int getMsgCount()
    {
        string sql = "SELECT * FROM QGetMsgBuyer WHERE buyerId = " + Session["usrID"];
        int sumMsg = 0;
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);
        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {
                while (oReader.Read())
                {
                    sumMsg += 1;
                }
                conn.Close();
            }
            com.Dispose();
            conn.Close();
        }
        return sumMsg;
    }
}