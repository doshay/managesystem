﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.master" AutoEventWireup="true" CodeFile="editUser.aspx.cs" Inherits="editUsr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="container rounded mt-3 p-2" style="background-color: white; min-height: 40rem;">
            <form method="post">
            <div class="row">
                <div class="col-8">
                                <h2 class="p-2">עריכת משתמש מספר <%=Request.QueryString["id"] %></h2>

                </div>
                <div class="col-4">
                      <button type="submit" name="submit" class="btn btn-warning btn-block">בצע עריכה</button>
                </div>
            </div>
            
                <div class="form-row">
                    
                                             <%=showUsrFields() %>

                  
                </div>

            </form>
         </div>
</asp:Content>

