﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        //בדיקה אם משתמש מחובר העברה לדף הבית
        if (Session["usrID"] != null)
        {
            Response.Redirect("Home.aspx");
        }
      
        //אם הוכנסו פרטים לטופס , הכנה למסד נתונים
        if (Request.Form["submit"] != null)
        {

            string email = Request.Form["email"];
            string pass = Request.Form["pass"];
            

            //Check if usr exist in server
            string SQL = "SELECT * FROM users WHERE pass =" + "'" + pass + "' AND (email ='" + email + "' OR uname = '"+ email+"')";


            string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
            path += "db.accdb";
            //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
            string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
            OleDbConnection conn = new OleDbConnection(connString);


            conn.Open();
            OleDbCommand com = new OleDbCommand(SQL, conn);
            OleDbDataReader data = com.ExecuteReader();
            bool found;
            found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
            data.Close();
            if (found)
            {
                using (OleDbDataReader oReader = com.ExecuteReader())
                {
                    //אם המשתמש מתאים למשתמש במסד הנתונים תחל הכניסה ויוכנסו לסשן משתנים מסוימים להמשך השהייה באתר
                    while (oReader.Read())
                    {
                        Session["usrEmail"] = oReader.GetString(3);
                        Session["usrTel"] = oReader.GetString(7);
                        
                        Session["usrID"] = oReader.GetInt32(0);
                        Session["pointsCounter"] = oReader.GetInt32(8);
                        if (oReader.GetBoolean(12))
                        {
                            Session["isAdmin"] = true;
                            Response.Redirect("AdminHome.aspx");
                        }
                        else
                            Session["isAdmin"] = false;

                        Response.Redirect("Home.aspx");
                    }

                    conn.Close();
                }

                com.Dispose();
                conn.Close();

                
            }
            else
            {
                AlertMsgServer.InnerHtml = "האימייל או הסיסמה לא נכונים אנא נסו שנית.";
            }
            
        }
    }
}