﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Search1 : System.Web.UI.Page
{
    public string SERVERSIDE_SubjectVal = "";
    public bool queryFilled = true;
    public string url = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["usrID"] == null)
        {
            Response.Redirect("Login.aspx");
        }

        if(Request.QueryString["subjectField"] != null)
             SERVERSIDE_SubjectVal = GetOptions("SELECT * FROM subSubjects WHERE SubjectId = " + Request.QueryString["subjectField"], Request.QueryString["subField"]);
        

        foreach (string x in Request.QueryString)
        {
            if(Request.QueryString[x] == "null")
            {
                queryFilled = false;
            }
        }
        url = fullUrl();
    }
    public string fullUrl()
    {
        string full = "Search.aspx";
        if(Request.QueryString != null)
        {
            full += "?";
        foreach (string x in Request.QueryString)
            {
                full += x + "=" + Request.QueryString[x] + "&";
                
            }
        }
        return full;

    }
    public string todayDate()
    {
        return DateTime.Now.ToString("d/M/yyyy");
    }

    public string GetOptions(string sql, string selected)
    {
        String finalString = "";
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);


        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {
                while (oReader.Read())
                {

                    bool isSelected = false;
                    if (selected != null)
                    {

                        if (selected == oReader.GetInt32(0).ToString())
                        {
                            isSelected = true;
                        }

                    }
                    if (isSelected)
                    {
                        finalString += "<option value=" + oReader.GetInt32(0) + " selected>" + oReader.GetString(1) + "</option>";
                    }
                    else
                    {
                        finalString += "<option value=" + oReader.GetInt32(0);
                        finalString += ">" + oReader.GetString(1) + "</option>";

                    }




                }

                conn.Close();
            }

            com.Dispose();
            conn.Close();

        }
        return finalString;
    }


    protected void SideFunctionLoadSub(object sender, EventArgs e)
    {

    }
    public string GetOptionsArea(string sql,string selected)
    {
        string finalString = "";
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);


        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {
                string optGroup = "";
                
                while (oReader.Read())
                {
                    if(optGroup == "" || oReader.GetString(0) != optGroup)
                    {
                        optGroup = oReader.GetString(0);                       
                        finalString += "</optgroup><optgroup label='"+optGroup+"'>";
                    }

                    bool isSelected = false;
                    if (selected != null)
                    {

                        if (selected == oReader.GetInt32(1).ToString())
                        {
                            isSelected = true;
                        }

                    }
                    if (isSelected)
                    {
                        finalString += "<option value=" + oReader.GetInt32(1) + " selected>" + oReader.GetString(2) + "</option>";
                    }
                    else
                    {
                        finalString += "<option value=" + oReader.GetInt32(1);
                        finalString += ">" + oReader.GetString(2) + "</option>";

                    }


                }

                conn.Close();
            }

            com.Dispose();
            conn.Close();

        }
        return finalString;
    }
    public string getFavorsList(string sql)
    {
        string finalString = "";
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);
        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {
                while (oReader.Read())
                {
                    /*<div class="col-4 p-1">
                             <div dir="rtl" class="card" style="width:fit-content;">
                                 <div class="card-body">
                                     <h5 class="card-title">שם טובה</h5>
                                     <h6 class="card-subtitle mb-2 text-muted">שם תת תחום</h6>
                                     <p class="card-text">מידע קצר על הטובה, כדי לדעת מה היא, עד 15 - 20 מילים</p>
                                     <div class="row">
                                         <div class="col-6 text-center">
                                      <p class="text-muted">ישראל ישראלי</p>

                                         </div>
                                         <div class="col-6 text-center">
                                             <p class="text-muted">20/4/2019</p>
                                         </div>
                                     </div>
                                     <button style="font-size:40px; color:white;" class="btn btn-warning btn-block font-weight-bold"><img src="img/icons/coin-stack.png" class="coins mr-5" />300</button>


                                 </div>
                             </div>
                         </div>
                         */
                    string buyStr = "favorListed.aspx?FID=" + oReader.GetInt32(0);
                    string profileStr = "profile?FID=" + oReader.GetInt32(14);


                    finalString += "<div class='col-4 p-1 d-flex align-items-stretch'>   <div dir='rtl' class='card' style='width:-webkit-fill-available;'><div class='card-body d-flex flex-column'>";
                    finalString+= "<h5 class='card-title'>"+oReader.GetString(4)+"</h5>";
                    finalString+= "<h6 class='card-subtitle mb-2 text-muted'>"+oReader.GetString(13)+"</h6>";
                    finalString+= "<p class='card-text'>"+oReader.GetString(11)+"</p>";
                    finalString += "<div class='mt-auto'><div class='row'>  <div class='col-6 text-center'>";
                    finalString += "<a class='text-muted' href='"+profileStr+"'>"+oReader.GetString(5)+ " " + oReader.GetString(6) +"</a> </div>";
                    finalString += "<div class='col-6 text-center'> <p class='text-muted'>"+ String.Format("{0:d/M/yyyy}", oReader.GetDateTime(2)) +"</p></div> </div>";


                    finalString += "<a role='button' href='"+buyStr+"' style='font-size:40px;color:white;' class='btn btn-warning btn-block font-weight-bold'><img src='img/icons/coin-stack.png' class='coins mr-2' />" + oReader.GetInt32(1) + "</a>";
                    finalString += "</div></div></div> </div>";



                }
                conn.Close();
            }
            com.Dispose();
           conn.Close();
        }
        return finalString;
    }
    public string CheckDate(string identifier)
    {
        if(Request.QueryString[identifier] != "null")
        {
            return Request.QueryString[identifier];
        }else
        {
            return todayDate();
        }
    }
}