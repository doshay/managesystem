﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Menu.master" AutoEventWireup="true" CodeFile="favorListed.aspx.cs" Inherits="FavorListed" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div class="container mt-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-8">
                    <h1 style="font-size: 2.5rem;">הזמנה מספר <%=Request.QueryString["FID"] %></h1>
                </div>
                <div class="col-4">
                    <button class="btn btn-warning btn-block" data-toggle="modal" data-target="#popUpConfirm">פנה למוכר</button>

                </div>
            </div>
            
            <div class="modal fade" id="popUpConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" dir="rtl">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">אתה בטוח?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            ברגע שהמוכר ייאשר את ההזמנה הנקודות ירדו מהחשבון שלך.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">ביטול</button>
                           
                            <a role="button" class="btn btn-warning"  href="<%=Request.Url + "&action=add" %>" data-dismiss="modal" data-toggle="modal" data-target="#popUp" >אני בטוח</a>
                              
                        </div>
                    </div>
                </div>
            </div>
             <div class="modal fade" id="popUp" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" dir="rtl">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <form runat="server">
                            <h5 runat="server" id="msgAfterSend"  class="modal-title">ההזמנה נשלחה ותוכל לעקוב אחריה <a href="#">כאן</a></h5>
                             </form>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">סגור</button>
                        </div>
                    </div>
                </div>
            </div>
             
            <hr class="my-3" style="border-color: white;" />
            <div class="container text-white ">
                <%=get() %>
                <div class="container p-3">
                    <p class="text-white">
                        לאחר ביצוע ההזמנה, מבקש הטובה יוכל לבדוק את פרטיך ולבצע אישור שלאחריו תוכל לתקשר עם המוכר,
                        <br />
                        האישור תתבצע ההזמנה והנקודות יעברו בין המוכר אליך הקונה.
                    </p>
                    <p class="text-white"><span class="font-weight-bold">שים לב!</span> ,לאחר אישור שלך ואישור המוכר לא תוכל לבצע החזר , אלא רק במקרים חריגים.</p>
                </div>

            </div>
        </div>
    </div>
</asp:Content>

