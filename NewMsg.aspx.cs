﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NewMsg : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Form["submit"] != null)
        {
            int unlistedFavor = 0;
            bool privacyCheck = false; ;
            string favorId;
            string txt = "";
            bool setTxt = false;
            favorId = Request.Form["favorId"];

            if (Request.Form["msg-text"] != null)
            {
                txt = Request.Form["msg-text"];
                setTxt = true;
            }

            if (setTxt)
            {
                //check if listed id match the user id connnected for security reason
                string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
                path += "db.accdb";
                //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
                string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
                OleDbConnection conn = new OleDbConnection(connString);

                String SQL = "SELECT userId,favorId FROM QGetInfoOfListed WHERE ListedId = " + favorId;
                conn.Open();
                OleDbCommand com = new OleDbCommand(SQL, conn);
                OleDbDataReader data = com.ExecuteReader();
                bool found;
                found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
                data.Close();
                if (found)
                {
                    using (OleDbDataReader oReader = com.ExecuteReader())
                    {
                        while (oReader.Read())
                        {
                            if (oReader.GetInt32(0) == (int)Session["usrID"])
                            {
                                privacyCheck = true;
                                //get unlisted favor number to acces the tbl
                                unlistedFavor = oReader.GetInt32(1);
                            }

                            

                        }

                        conn.Close();
                    }

                    com.Dispose();
                    conn.Close();


                }
                if (privacyCheck)
                {
                    Dbb.DoQuery("db.accdb", "UPDATE favorsUnlisted SET deleted = TRUE WHERE favorId = " + unlistedFavor);
                    Dbb.DoQuery("db.accdb", "UPDATE FavorListed SET unList = TRUE WHERE ListedId =" + favorId);
                    string tempSQL = "INSERT INTO usrMsg(ListedId,msg,usrId) VALUES(" + favorId + ",'" + txt + "'," + Session["usrId"] + ")";
                    Dbb.DoQuery("db.accdb", tempSQL);
                }
            }
            else
            {
                Dbb.DoQuery("db.accdb", "UPDATE FavorListed SET unList = TRUE WHERE ListedId" + favorId);

            }


        }
    }
    public string getNewFavors()
    {
        String sql = "SELECT SubjectName,subName,shortName,describeText,ListedId,fname,lname,id,[level],timesRated,timeCreated,isSeen,price,unList FROM QGetInfoOfListed WHERE unList = FALSE AND userId = "+Session["usrID"] ;

        string finalString = "";
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);


        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {               
                while (oReader.Read())
                {
                    /*
                     *  <div class="card">
                        <div class="card-header">
                            עזרה ברכבים - תיקון פאנצר
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">עזרה דחוףף יש פאנצר באוטו!</h5>
                            <div class="row">
                                <div class="col-6">
                            <p class="card-text">אני חייב עזרה זה ממה שדחוף</p>

                                </div>
                                <div class="col-6">
                                    <div class="container">
                                        <p></p>
                                        <p class="text-muted mb-0"> שי ליבני - מאסטר (23 דירוגים) </p>
                                        <span class='fa fa-star checked'></span>
                                        <span class='fa fa-star checked'></span>
                                        <span class='fa fa-star checked'></span>

                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                     <a href="#" class="btn btn-warning mt-2">אפשרויות נוספות</a>
                                </div>
                                <div class="col-8">
                                </div>
                                <div class="col-2  d-flex align-items-end">
                                    <a class="text-muted">24/12/2018</a>
                                </div>
                            </div>
                        </div>
                    </div>
*/
                    finalString += "<div class='card'><div class='card-header'>";
                    if(oReader.GetBoolean(13))
                        finalString += oReader.GetString(0) + " " + oReader.GetString(1);
                    else
                        finalString += oReader.GetString(0) + " " + oReader.GetString(1) + "  &nbsp; <span style='font-size:1.4rem; background-color:#ffb900;' class='badge badge-light font-weight-bold'>פנייה חדשה!</span>";

                    finalString += "</div>  <div class='card-body'> <h5 class='card-title'>"+oReader.GetString(2) +"</h5>";
                    finalString += " <div class='row'><div class='col-6'><p class='card-text'>"+oReader.GetString(3) +"</p>";
                    finalString += "</div><div class='col-6'><div class='container'><p dir='rtl' class='text-muted mb-0'>"+oReader.GetString(5) +" "+ oReader.GetString(6) + "("+oReader.GetInt32(9)+")";
                    int temp = 0;
                    for (int i = 0; i < oReader.GetInt32(8); i++)
                    {
                        finalString += "<span class='fa fa-star checked'></span>";
                        temp += 1;
                    }
                    for (int i = temp; i < 5; i++)
                    {
                        finalString += "<span class='fa fa-star'></span>";

                    }
                    finalString += "</div></div></div>";
                    finalString += " <div class='row'><div class='col-2'><a href='contactOptions.aspx?FID="+oReader.GetInt32(4)+ "' class='btn btn-warning btn-block mt-2'>אפשרויות נוספות</a></div><div class='col-2'><button data-toggle='modal' data-target='#exampleModal' data-id='"+oReader.GetInt32(4)+"' data-seller='"+oReader.GetString(5) +" "+oReader.GetString(6) +"' data-points="+oReader.GetInt32(12)+" class='btn btn-success btn-block mt-2'>אישור מהיר</button></div><div class='col-6'></div><div class='col-2  d-flex align-items-end'>";
                    finalString += " <a class='text-muted'>"+oReader.GetDateTime(10).ToString("dd/MM/yy")+"</a>";
                    finalString += "</div></div></div>";
                }

                conn.Close();
            }

            com.Dispose();
            conn.Close();

        }
        else
        {
            finalString += "<h2 class='text-white text-center'>אין פניות חדשות להצגה</h2>";
        }
        return finalString;
    }
    public string getNewMsg()
    {
        String sql = "SELECT * FROM QGetMsgBuyer WHERE buyerId  = "+Session["usrID"];

        string finalString = "";
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);


        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {
                while (oReader.Read())
                {
                    /*
                     *  <div class="card">
                        <div class="card-header">
                            עזרה ברכבים - תיקון פאנצר
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">עזרה דחוףף יש פאנצר באוטו!</h5>
                            <div class="row">
                                <div class="col-6">
                            <p class="card-text">אני חייב עזרה זה ממה שדחוף</p>

                                </div>
                                <div class="col-6">
                                    <div class="container">
                                        <p></p>
                                        <p class="text-muted mb-0"> שי ליבני - מאסטר (23 דירוגים) </p>
                                        <span class='fa fa-star checked'></span>
                                        <span class='fa fa-star checked'></span>
                                        <span class='fa fa-star checked'></span>

                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2">
                                     <a href="#" class="btn btn-warning mt-2">אפשרויות נוספות</a>
                                </div>
                                <div class="col-8">
                                </div>
                                <div class="col-2  d-flex align-items-end">
                                    <a class="text-muted">24/12/2018</a>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="card">
                        <div class="card-header">
                            הודעה מאת - שי ליבני
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">בקשר למודעה - דחוף! פנצ'ר באוטו</h5>
                            <div class="row">
                                <div class="col-6">
                                    <p class="card-text">כשדגכככככככדגכגדכדג דכדגכדגכדגכדגכ כדגכדגכדגכ דגכדגכדגכדג דכדגכאני חייב עזרה זה ממה שדחוף</p>

                                </div>
                                <div class="col-6">
                                    <div class="container">
                                        
                                        <p class="text-muted mb-0">שי ליבני - מאסטר (23 דירוגים) </p>
                                        <span class='fa fa-star checked'></span>
                                        <span class='fa fa-star checked'></span>
                                        <span class='fa fa-star checked'></span>

                                    </div>
                                    <div class="container">
                                       

                                        <button class="btn btn-secondary btn-block">אפשרויות נוספות</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
*/
                    finalString += "<div class='card'><div class='card-header'>";

                    finalString += "הודעה מאת - " + oReader.GetString(7) + " " + oReader.GetString(8);


                    finalString += "</div>  <div class='card-body'> <h5 class='card-title'>" + "בקשר למודעה - " + oReader.GetString(6) + "</h5>";
                    finalString += " <div class='row'><div class='col-6'><p class='card-text'>" + oReader.GetString(2) + "</p>";
                    finalString += "</div><div class='col-6'><div class='container'><p dir='rtl' class='text-muted mb-0'>" + oReader.GetString(7) + " " + oReader.GetString(8) + "(" + oReader.GetInt32(10) + ")";
                    int temp = 0;
                    finalString += "<div>";
                    for (int i = 0; i < oReader.GetInt32(11); i++)
                    {
                        finalString += "<span class='fa fa-star checked'></span>";
                        temp += 1;
                    }
                    for (int i = temp; i < 5; i++)
                    {
                        finalString += "<span class='fa fa-star'></span>";

                    }
                    finalString += "</div>";

                    finalString += "</div>";
                    finalString += "<div class='container'>";
                    finalString += "<a role='button' href='deleteMsg.aspx?MID="+oReader.GetInt32(0)+"' class='btn btn-danger btn-block mt-1 pb-1'>מחק</a>";
                    finalString += "<button type='button' class='btn btn-secondary btn-block mt-1 pb-1'>אפשרויות </button>";
                    finalString += "</div></div></div></div></div>";
                }

                conn.Close();
            }

            com.Dispose();
            conn.Close();

        }
        else
        {
            finalString += "<h2 class='text-white text-center'>אין הודעות להצגה.</h2>";
        }
        return finalString;
    }

}