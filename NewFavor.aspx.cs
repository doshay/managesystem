﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NewFavor : System.Web.UI.Page
{

    //global subject info
    public int subid = 0;
    public string subName = "";
    public string imgH = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["usrID"] == null)
        {
            Response.Redirect("Login.aspx");
        }
        if (Request.QueryString["SID"] == null)
        {
            ASubjectPanel.Visible = false;
            SubjectPanel.Visible = true;
        }else
        {
            getSubInfo();
            ASubjectPanel.Visible = true;
            SubjectPanel.Visible = false;
        }

        if (Request.Form["submit"] != null)
        {
            string city,now,date,shortTxt,longTxt,sub;
            sub = Request.Form["subField"];
            longTxt = Request.Form["freeTxt"];
            shortTxt = Request.Form["shortName"];
            now = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");           
            city = Request.Form["cityField"];
            date = Request.Form["DateField"];


            //insert details into user account
            string sql = "INSERT INTO favorsUnlisted(timeCreated,subId,AreaId,timeEvent,userId,describeText,shortName,price) VALUES(";
            sql += "'" + now + "',";
            sql += "'" + sub + "',";
            sql += "'" + city + "',";
            sql += "'" + date + "',";
            sql += "'" + Session["usrID"] + "',";
            sql += "'"+longTxt+"',";
            sql += "'"+shortTxt+"',";
            sql += 200 +")";
            Dbb.DoQuery("db.accdb", sql);
            //Response.Write(sql);
        }
    }
    public string getSubjects(string sql)
    {
        string finalString = "";
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);
        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {
                while (oReader.Read())
                {
                    /*<div class="col-4 pb-2 d-flex flex-column align-items-center">
                    <div class="card " style="width: auto; min-height:26rem;">
                        <img class="card-img-top" src="img/thumbnails/school.jpg" alt="Card image cap" />
                        <div class="card-body text-center d-flex flex-column">
                            <h5 class="card-title">בית ספר ולימודים</h5>
                            <p class="card-text">עזרה עם שיעורי בית, שיעורים פרטיים ועוד...</p>

                            <div class="mt-auto">
                                <a href="#" class="btn btn-outline-warning">Go somewhere</a>
                            </div>
                        </div>
                    </div>
                </div>
                         */
                    
                        string chooseStr = "NewFavor.aspx?SID=" + oReader.GetInt32(0);
                        string thumbnailHref = "img/thumbnails/" + oReader.GetString(2);


                        finalString += "<div class='col-4 p-2 d-flex align-items-center'>   <div  class='card' style='width:auto; min-height:26rem;'>";
                        finalString += "<img class='card-img-top' src='" + thumbnailHref + ".jpg' alt=" + oReader.GetString(1) + " />";
                        finalString += "<div class='card-body text-center d-flex flex-column'>";
                        finalString += "<h5 class='card-title'>" + oReader.GetString(1) + "</h5>";
                        finalString += "<p class='card-text'>" + oReader.GetString(3) + "</p>";
                        finalString += "<div class='mt-auto'>";


                        finalString += "<a role='button' href='" + chooseStr + "' class='btn btn-outline-warning btn-block'> בחר בתחום</a>";
                        finalString += "</div> </div> </div></div>";
                    

                    


                }
                conn.Close();
            }
            com.Dispose();
            conn.Close();
        }
        return finalString;
    }
    public string GetOptions(string sql, string selected)
    {
        String finalString = "";
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);


        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {
                while (oReader.Read())
                {

                    bool isSelected = false;
                    if (selected != null)
                    {

                        if (selected == oReader.GetInt32(0).ToString())
                        {
                            isSelected = true;
                        }

                    }
                    if (isSelected)
                    {
                        finalString += "<option value= "+ oReader.GetInt32(0) +"  selected>" + oReader.GetString(1) + "</option>";
                    }
                    else
                    {
                        finalString += "<option value=" + oReader.GetInt32(0);
                        finalString += ">" + oReader.GetString(1) + "</option>";

                    }




                }

                conn.Close();
            }

            com.Dispose();
            conn.Close();

        }
        return finalString;
    }

    public string GetOptionsArea(string sql, string selected)
    {
        string finalString = "";
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);


        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {
                string optGroup = "";

                while (oReader.Read())
                {
                    if (optGroup == "" || oReader.GetString(0) != optGroup)
                    {
                        optGroup = oReader.GetString(0);
                        finalString += "</optgroup><optgroup label='" + optGroup + "'>";
                    }

                    bool isSelected = false;
                    if (selected != null)
                    {

                        if (selected == oReader.GetInt32(1).ToString())
                        {
                            isSelected = true;
                        }

                    }
                    if (isSelected)
                    {
                        finalString += "<option value=" + oReader.GetInt32(1) + " selected>" + oReader.GetString(2) + "</option>";
                    }
                    else
                    {
                        finalString += "<option value=" + oReader.GetInt32(1);
                        finalString += ">" + oReader.GetString(2) + "</option>";

                    }


                }

                conn.Close();
            }

            com.Dispose();
            conn.Close();

        }
        return finalString;
    }

    public void getSubInfo()
    {
        string sql = "SELECT * FROM subjects WHERE subjectId = " + Request.QueryString["SID"];
        string path = HttpContext.Current.Server.MapPath("db/");//מיקום מסד בפורוייקט
        path += "db.accdb";
        //string path = HttpContext.Current.Server.MapPath("App_Data/" + fileName);//מאתר את מיקום מסד הנתונים מהשורש ועד התקייה בה ממוקם המסד
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data source=" + path;//נתוני ההתחברות הכוללים מיקום וסוג המסד
        OleDbConnection conn = new OleDbConnection(connString);
        conn.Open();
        OleDbCommand com = new OleDbCommand(sql, conn);
        OleDbDataReader data = com.ExecuteReader();
        bool found;
        found = (bool)data.Read();// אם יש נתונים לקריאה יושם אמת אחרת שקר - הערך קיים במסד הנתונים
        data.Close();
        if (found)
        {
            //Found user matching
            using (OleDbDataReader oReader = com.ExecuteReader())
            {
                while (oReader.Read())
                {
                    
                        subid = oReader.GetInt32(0);
                        subName = oReader.GetString(1);
                        imgH = oReader.GetString(2);
                    
                }
                conn.Close();
            }
            com.Dispose();
            conn.Close();
        }
       

    }
}